#ifndef PSYCHOAPPLOGIC_HPP
#define PSYCHOAPPLOGIC_HPP

#include <QObject>

#include <QHash>
#include <QVector>
#include <QDir>

class PsychoAppLogic : public QObject
{
  Q_OBJECT

public:
  enum KeyPressed
  {
    Up,
    Down
  };

typedef QPair<int, KeyPressed>    UserDataRecord;
typedef QVector<UserDataRecord>   UserData;
typedef QHash<QString, UserData>  FilmData;

public:
  explicit PsychoAppLogic(QObject *parent = 0);

signals:

public slots:
  bool exists(const QString& userId) const;
  bool newUser(const QString& newUserId);
  bool setCurrentFilm(const QString& filmPath);
  bool setOutputDir(const QString& outputDirPath);
  bool start();
  bool upKeyPressed(const int duration);
  bool downKeyPressed(const int duration);
  bool save();

private:
  QDir      _outputDir;
  QString   _currentUser;
  QString   _currentFilm;
  FilmData  _films;
  UserData* _currentFilmData;
};

#endif // PSYCHOAPPLOGIC_HPP
