## Add more folders to ship with the application, here
#folder_01.source = qml/VideoPlayer
#folder_01.target = qml
#DEPLOYMENTFOLDERS = folder_01

## Additional import path used to resolve QML modules in Creator's code model
#QML_IMPORT_PATH =

TEMPLATE = app
QT += quick multimedia

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    psychoapplogic.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
#qtcAddDeployment()

HEADERS += \
    psychoapplogic.hpp

RESOURCES += \
    PsychoApp.qrc

OTHER_FILES += \
    qml/main.qml \
    qml/NewUserDialog.qml \
    qml/PsyButton.qml \
    qml/PsyButtonContainer.qml \
    qml/PsychoApp.qml \
    qml/PsyVideoPlayer.qml \
    qml/SideBar.qml \
    qml/VideoPlayer.qml \
    qml/Line.qml \
    qml/Arrow.qml
