#include "psychoapplogic.hpp"

#include <QTextStream>
#include <QUrl>
#include <QDebug>

PsychoAppLogic::PsychoAppLogic(QObject *parent) :
    QObject(parent),
    _outputDir(QDir::tempPath()),
    _currentUser(""),
    _currentFilm(""),
    _currentFilmData(0)
{}

bool PsychoAppLogic::exists(const QString& userId) const
{
  if(this->_outputDir.exists(userId+".data"))
    return true;

  return false;
}

bool PsychoAppLogic::newUser(const QString& userId)
{
  if(this->_currentUser==userId)
    return false;

  this->_currentUser=userId;

  return true;
}

bool PsychoAppLogic::setCurrentFilm(const QString& filmPath)
{
  QFileInfo filmFileInfo(QUrl(filmPath).path());
  if(!filmFileInfo.exists())
    return false;

  QString filmId=filmFileInfo.fileName();
  FilmData::Iterator find_it=this->_films.find(filmId);
  if(find_it!=this->_films.end())
  {
    Q_ASSERT(find_it.key()==filmId);
    this->_currentFilm=filmId;
    this->_currentFilmData=&(find_it.value());
  }
  else
  {
    find_it=this->_films.insert(filmId, UserData());
    Q_ASSERT(find_it.key()==filmId);
    this->_currentFilm=filmId;
    this->_currentFilmData=&(find_it.value());
  }

  return true;
}

bool PsychoAppLogic::setOutputDir(const QString& outputDirPath)
{
  this->_outputDir=QDir(QUrl(outputDirPath).path());
  if(this->_outputDir.exists())
    return true;

  this->_outputDir=QDir::tempPath();

  return false;
}

bool PsychoAppLogic::start()
{
  if(!this->_currentFilmData)
    return false;

  this->_currentFilmData->clear();

  return true;
}

bool PsychoAppLogic::upKeyPressed(const int duration)
{
  if(!this->_currentFilmData)
    return false;

  this->_currentFilmData->push_back(qMakePair(duration, Up));

  qDebug()<<"PsychoAppLogic::upKeyPressed(const int duration) with duration:"
            <<duration;

  return true;
}

bool PsychoAppLogic::downKeyPressed(const int duration)
{
  if(!this->_currentFilmData)
    return false;

  this->_currentFilmData->push_back(qMakePair(duration, Down));

  qDebug()<<"PsychoAppLogic::downKeyPressed(const int duration) with duration:"
            <<duration;

  return true;
}

QString pressedKeyToString(const PsychoAppLogic::KeyPressed keyPressed)
{
  if(keyPressed==PsychoAppLogic::Up)
    return "+1";
  else if(keyPressed==PsychoAppLogic::Down)
    return "-1";

  return "";
}

bool PsychoAppLogic::save()
{
  QString fileName=this->_currentUser+".data";
  QFile data(this->_outputDir.absoluteFilePath(fileName));
  if (data.open(QFile::WriteOnly | QFile::Truncate))
  {
    QTextStream out(&data);
    out<<"user"<<"\t"<<"film"<<"\t"<<"time(ms)"<<"\t"<<"value"<<"\n";

    for(FilmData::const_iterator f_it=this->_films.begin();
        f_it!=this->_films.end();
        ++f_it)
    {
      foreach(const UserDataRecord& userDataRecord, f_it.value())
      {
        out << this->_currentUser << "\t"
               << f_it.key()<<"\t"
                  <<QString("%1").arg(userDataRecord.first)<<"\t"
                    <<pressedKeyToString(userDataRecord.second)<<"\n";
      }
    }

    qDebug()<<"user data for user:'"<<this->_currentUser
           <<"' were saved to file "<<data.fileName();

    return true;
  }

  return false;
}
