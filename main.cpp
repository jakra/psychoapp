#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"

#include "psychoapplogic.hpp"

#include <QQmlContext>

int main(int argc, char *argv[])
{
  Q_INIT_RESOURCE(PsychoApp);
  QGuiApplication app(argc, argv);

  QtQuick2ApplicationViewer viewer;
  viewer.setSource(QStringLiteral("qrc:/qml/main.qml"));
  viewer.setTitle("Sascha's PsychoApp");

  PsychoAppLogic psychoAppLogic;
  QQmlContext *ctxt = viewer.rootContext();
  ctxt->setContextProperty("appLogic", &psychoAppLogic);

  viewer.showExpanded();

  return app.exec();
}
