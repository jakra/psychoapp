import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

Rectangle {
    id: newUserDialog
    width: 200
    height: 150
    radius: 15
    border.width: 3
    border.color: "lightgrey"
    color: "darkgrey"

    property string userID: newUserIdInput.displayText

    signal okClicked();
    signal cancelClicked();

    onVisibleChanged:
    {
      if(visible==true)
        newUserIdInput.focus=true;
      else
      {
        if(parent)
          parent.focus=true;
      }
    }

    Column{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 10

        Label {
            id: newUserLabel
            height: newUserDialog.height/4
            width: newUserDialog.width
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            font.pointSize: 20
            text: qsTr("(new) user id:")
        }

        TextField{
            id: newUserIdInput
            height: newUserDialog.height/4
            width: newUserDialog.width
            horizontalAlignment: Text.AlignHCenter
        }

        Rectangle{
            id: dialogButtonsBar

            color: "transparent"

            height: newUserDialog.height/4
            width: newUserDialog.width

            Row{
                id: dialogButtonRow
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                spacing: 10

                Button {
                    id: okButton
                    text: "OK"
                    width: 50
                    height: 25
                    style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100
                            implicitHeight: 25
                            border.width: control.activeFocus ? 2 : 1
                            border.color: "#888"
                            radius: 4
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                            }
                        }
                    }
                    onClicked: newUserDialog.okClicked();
                }

                Button {
                    id: cancelButton
                    text: "Cancel"
                    width: 50
                    height: 25
                    style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100
                            implicitHeight: 25
                            border.width: control.activeFocus ? 2 : 1
                            border.color: "#888"
                            radius: 4
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                            }
                        }
                    }
                    onClicked: newUserDialog.cancelClicked();
                }
            }
        }
    }
}
