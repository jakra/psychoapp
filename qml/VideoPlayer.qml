import QtQuick 2.0
import QtMultimedia 5.0

Item {
    id: videoPlayer
    property string videoFile: ""
    property int playbackState: mediaplayer.playbackState
    property int position: mediaplayer.position

    signal videoStopped();
    signal videoStarted();
    signal videoPaused();

    MediaPlayer {
        id: mediaplayer
        source: videoPlayer.videoFile
        onStopped: videoPlayer.videoStopped()
        onPlaying: videoPlayer.videoStarted()
        onPaused: videoPlayer.videoPaused()
    }

    VideoOutput {
        anchors.fill: parent
        source: mediaplayer
    }

    function play()   { mediaplayer.play() }
    function pause()  { mediaplayer.pause() }
    function stop()   { mediaplayer.stop() }
}
