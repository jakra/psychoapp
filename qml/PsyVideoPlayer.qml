import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import QtMultimedia 5.0

Rectangle {
  id: psyVideoPlayer
  width: 100
  height: 62
  color: "darkred"

  property string videoFile: ""

  property int position: videoPlayer.position

  property Action loadVideoAction: loadVideoAct;
  property Action startVideoAction: startVideoAct
  property Action stopVideoAction: stopVideoAct

  signal videoLoaded();
  signal videoStarted();
  signal videoPaused();
  signal videoStopped();

  Action {
    id: loadVideoAct
    enabled: false

    iconSource: "qrc:/icons/film.png"
    tooltip: qsTr("Open Video")

    onTriggered: fileDialog.open()

    onEnabledChanged:
    {
      if(loadVideoAction.enabled==true)
      {
        videoLabelRect.visible=true
        videoLabelRect.height=40
      }
      else
      {
        videoLabelRect.visible=false;
        videoLabelRect.height=0
      }
    }
  }

  Action {
    id: startVideoAct

    iconSource: "qrc:/icons/play.png"
    tooltip: qsTr("Start Video")

    onTriggered:
    {
      if(videoPlayer.playbackState == MediaPlayer.PlayingState)
      {
        videoPlayer.pause()
        videoPaused()
        iconSource="qrc:/icons/play.png"
        console.log("signal videoPaused sent.")
      }
      else
      {
        videoPlayer.play()
        if(videoPlayer.playbackState == MediaPlayer.PlayingState)
        {
          videoStarted()
          iconSource="qrc:/icons/pause.png"
          console.log("signal videoStarted sent.")
        }
        else
        {
          console.error("video: "
                        +psyVideoPlayer.videoFile
                        +" could not be started!")
        }
      }
    }
  }

  Action {
    id: stopVideoAct

    iconSource: "qrc:/icons/stop.png"
    tooltip: qsTr("Stop Video")

    onTriggered:
    {
      videoPlayer.stop()
      startVideoAct.iconSource="qrc:/icons/play.png"
    }
  }

  VideoPlayer
  {
    id: videoPlayer
    anchors.top: parent.top
    anchors.topMargin: 5
    anchors.left: parent.left
    anchors.leftMargin: 5
    anchors.right: parent.right
    anchors.rightMargin: 5
    anchors.bottom: toolBar.top
    anchors.bottomMargin: 5

    onVideoStarted: 
    {
      psyVideoPlayer.videoStarted()
    }
    
    onVideoPaused: 
    {
      psyVideoPlayer.videoPaused()
    }

    onVideoStopped: 
    {
      psyVideoPlayer.videoStopped();
      startVideoAct.iconSource="qrc:/icons/play.png"
    }
  }

  Rectangle {
    id: toolBar
    height: 40
    radius: 5
    color: "transparent"
    visible: false

    anchors.left: parent.left
    anchors.leftMargin: 5
    anchors.right: parent.right
    anchors.rightMargin: 5
    anchors.bottom: videoLabelRect.top
    anchors.bottomMargin: 5

    RowLayout {
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.verticalCenter: parent.verticalCenter

      ToolButton {
        id: startButton
        action: startVideoAction
      }

      ToolButton {
        id: stopButton
        action: stopVideoAction
      }

    }
  }

  Rectangle{
    id: videoLabelRect
    visible: false

    color: "transparent"
    height: 40

    Behavior on height { PropertyAnimation {} }

    anchors.left: parent.left
    anchors.leftMargin: 5
    anchors.right: parent.right
    anchors.rightMargin: 5
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 5

    RowLayout {

      ToolButton {
        id: loadButton
        action: loadVideoAction
      }

      Label{
        id: videoLabel
      }
    }
  }

  FileDialog{
    id: fileDialog
    title: "Please choose a video file"
    onAccepted: {
      psyVideoPlayer.videoFile=fileDialog.fileUrl;
      videoLoaded()
      console.log("You chose: " + psyVideoPlayer.videoFile)
    }
    onRejected: {
      console.log("Cancelling choosing video file")
    }
  }

  onVideoFileChanged:
  {
    if(videoFile.trim().length==0)
    {
      toolBar.visible=false;
      videoLabel.text="";
      videoPlayer.videoFile=""
    }
    else
    {
      toolBar.visible=true;
      videoLabel.text=psyVideoPlayer.videoFile;
      videoPlayer.videoFile=psyVideoPlayer.videoFile
    }
  }
}
