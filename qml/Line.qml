import QtQuick 2.0

Canvas {
  id: lineCanvas

  width: 25
  height: 3

  property real x1: 1.0
  property real y1: 1.0
  property real x2: 2.0
  property real y2: 2.0

  onPaint:
  {
    var ctx = lineCanvas.getContext('2d');
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2,y2);
    ctx.setLineWidth(width);
  }
}
