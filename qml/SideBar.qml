import QtQuick 2.0
import QtQuick.Layouts 1.1

Rectangle {
    id: sideBar
    width: 150
    height: 62
    state: "ShownState"
    color: "transparent"

    property color buttonBorderColor: "#7F8487"
    property color buttonFillColor: "#8FBDCACD"

    signal newUser();
    signal openVideo();
    signal startVideo();
    signal upClicked();
    signal downClicked();

    Rectangle
    {
        id: clickSide
        width: 15
        color: "black"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        MouseArea {
          id: mouseArea
          anchors.fill: parent
          onClicked: {
              if (sideBar.state == "ClosedState")
                  sideBar.state = "ShownState"
              else if(sideBar.state == "ShownState")
                  sideBar.state = "ClosedState"
          }
      }
    }

    Rectangle
    {
        id: buttonContainer
        width: 75
        color: "transparent"
        anchors.left: clickSide.right
        anchors.leftMargin: 5
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5

        ColumnLayout {
            anchors.centerIn: parent
            spacing: 10

            Button {
                id: newButton
                label: qsTr("New \nUser")
                borderColor: buttonBorderColor
                buttonColor: buttonFillColor
                width: buttonContainer.width / 1.3
                height: 40
                labelSize: 12
                anchors.horizontalCenter: parent.horizontalCenter
                onButtonClick: {
                    sideBar.newUser();
                    console.log("button open video clicked.")
                }
            }

            Button {
                id: openButton
                label: qsTr("Open \nVideo")
                borderColor: buttonBorderColor
                buttonColor: buttonFillColor
                width: buttonContainer.width / 1.3
                height: 40
                labelSize: 12
                anchors.horizontalCenter: parent.horizontalCenter
                onButtonClick: {
                    sideBar.openVideo();
                    console.log("button open video clicked.")
                }
            }

            Rectangle{
                id: space1
                color: "transparent"
                Layout.fillHeight: true
            }

            Button {
                id: startButton
                label: qsTr("Start")
                borderColor: buttonBorderColor
                buttonColor: buttonFillColor
                width: buttonContainer.width / 1.3
                height: 40
                labelSize: 12
                anchors.horizontalCenter: parent.horizontalCenter
                onButtonClick: {
                    sideBar.startVideo();
                    console.log("start-button clicked.")
                }
            }

            Rectangle{
                id: space2
                color: "transparent"
                Layout.fillHeight: true
            }

            Button {
                id: upButton
                label: qsTr("Up")
                borderColor: buttonBorderColor
                buttonColor: buttonFillColor
                width: buttonContainer.width / 1.3
                height: 40
                labelSize: 12
                anchors.horizontalCenter: parent.horizontalCenter
                onButtonClick: {
                    sideBar.upClicked();
                    console.log("up-button clicked.")
                }
            }

            Button {
                id: downButton
                label: qsTr("Down")
                borderColor: buttonBorderColor
                buttonColor: buttonFillColor
                width: buttonContainer.width / 1.3
                height: 40
                labelSize: 12
                anchors.horizontalCenter: parent.horizontalCenter
                onButtonClick: {
                    sideBar.downClicked();
                    console.log("down-button clicked.")
                }
            }
        }
    }

    states: [
        State {
             name: "ShownState"

             PropertyChanges{target: sideBar; width: 100}
             PropertyChanges{target: buttonContainer; opacity: 1.0}
         },
         State {
             name: "ClosedState"

             PropertyChanges{target: sideBar; width: 25}
             PropertyChanges{target: buttonContainer; opacity: 0.0}
         }
     ]

    transitions: [
        Transition {
            to: "*"

            ParallelAnimation{
                NumberAnimation{target: sideBar; properties: "width"; duration: 1000}
                NumberAnimation{target: buttonContainer; properties: "opacity"; duration: 1000}
            }
        }
    ]
}
