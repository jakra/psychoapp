import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

Rectangle
{
  id: buttonContainer
  width: 85
  radius: 15
  color: "lightgrey"

  property color buttonBorderColor: "#7F8487"
  property color buttonFillColor: "#8FBDCACD"

  property Action newUserAction: newUserAct;
  property Action settingsAction: settingsAct;
  property Action upKeyAction: upKeyAct
  property Action downKeyAction: downKeyAct

  property Label userField: userLabel

  signal newUser();
  signal upClicked();
  signal downClicked();
  signal settingsClicked();

  function upKeyPressed()
  {
    console.log("up key pressed function of button container")
  }

  function downKeyPressed()
  {
    console.log("down key pressed function of button container")
  }

  Action {
    id: newUserAct

    iconSource: "qrc:/icons/old_man.png"
    tooltip: qsTr("New User")
    text: "user:"

    onTriggered:
    {
      newUser();
      console.log("new user executed.")
    }
  }

  Action {
    id: settingsAct

    iconSource: "qrc:/icons/settings.png"
    tooltip: qsTr("Settings")
    text: "settings"

    onTriggered:
    {
      settingsClicked();
      console.log("settings action executed.")
    }

    onEnabledChanged:
    {
      if(settingsAct.enabled==true)
        settingsButton.visible=true;
      else
        settingsButton.visible=false;
    }
  }

  Action {
    id: upKeyAct

    iconSource: "qrc:/icons/up_arrow.png"
    tooltip: qsTr("Press Up Key")

    onEnabledChanged:
    {
      if(upKeyAct.enabled==true)
        upButton.visible=true;
      else
        upButton.visible=false;
    }

    onTriggered:
    {
      upClicked();
      console.log("up key clicked.")
    }
  }

  Action {
    id: downKeyAct

    iconSource: "qrc:/icons/down_arrow.png"
    tooltip: qsTr("Press Down Key")

    onEnabledChanged:
    {
      if(downKeyAct.enabled==true)
        downButton.visible=true;
      else
        downButton.visible=false;
    }

    onTriggered:
    {
      downClicked();
      console.log("down key clicked.")
    }
  }

  Rectangle{
    id: topToolbar
    color: "transparent"

    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right

    height: 50

    Column{
      id: column
      anchors.horizontalCenter: parent.horizontalCenter
      spacing: 10

      ToolButton {
        id: newButton
        action: newUserAction
        anchors.horizontalCenter: parent.horizontalCenter
      }

      Label{
        id: userLabel
        anchors.horizontalCenter: parent.horizontalCenter
        width: topToolbar.width
        horizontalAlignment: Text.AlignHCenter
        visible: false
      }
    }
  }

  Rectangle{
    id: arrowsToolbar
    color: "transparent"

    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: settingsToolbar.top

    height: 100

    Column{
      anchors.horizontalCenter: parent.horizontalCenter
      spacing: 10

      ToolButton {
        id: upButton
        action: upKeyAction
      }

      ToolButton {
        id: downButton
        action: downKeyAction
      }

      move: Transition { NumberAnimation { properties: "x,y"; duration: 1000}}
    }
  }

  Rectangle{
    id: settingsToolbar
    color: "transparent"

    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom

    height: 50

    Column{
      anchors.horizontalCenter: parent.horizontalCenter
      spacing: 10

      ToolButton {
        id: settingsButton
        action: settingsAction
      }

      move: Transition { NumberAnimation { properties: "x,y"; duration: 1000}}
    }
  }

//  ColumnLayout {

//    anchors.horizontalCenter: parent.horizontalCenter

//    spacing: 10

//    ToolButton {
//      id: newButton
//      action: newUserAction
//    }

//    ToolButton {
//      id: openButton
//      action: openVideoAction
//    }

//    Rectangle{
//      id: space1
//      color: "transparent"
//      Layout.fillHeight: true
//    }

//    ToolButton {
//      id: upButton
//      action: upKeyAction
//    }

//    ToolButton {
//      id: downButton
//      action: downKeyAction
//    }
//  }
}
