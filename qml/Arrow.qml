import QtQuick 2.0

Canvas {
  id: arrowCanvas

  width: 10
  height: 10

  property int lineWidth: 3
  property int direction: Qt.UpArrow

  onPaint:
  {
    var ctx=arrowCanvas.context('2d');
    ctx.lineWidth=arrowCanvas.lineWidth;

    if(arrowCanvas.direction==Qt.UpArrow)
    {
      ctx.moveTo(0, height);
      ctx.lineTo(width/2, 0);
      ctx.lineTo(width, height);
    }
  }
}
