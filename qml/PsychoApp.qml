import QtQuick 2.0
import QtQuick.Dialogs 1.1

Rectangle {
  id: psychoApp

  width: 600
  height: 400
  color: "darkgrey"

  state: "InitState"

  property string userID: "";
  property string outputPath: "";

  function upKey()
  {
    if(psychoApp.state!="RunningState")
      return;

    appLogic.upKeyPressed(player.position);
    buttonContainer.upKeyPressed();
    console.log("up key pressed with position: " +player.position+" ms")

    return;
  }

  function downKey()
  {
    if(psychoApp.state!="RunningState")
      return;

    appLogic.downKeyPressed(player.position);
    buttonContainer.downKeyPressed();
    console.log("down key pressed with position: " +player.position+" ms")

    return;
  }

  PsyVideoPlayer{
    id: player
    color: "transparent"

    anchors.leftMargin: 5
    anchors.bottomMargin: 5
    anchors.topMargin: 5
    anchors.right: clickSide.left
    anchors.rightMargin: 5
    anchors.left: parent.left
    anchors.top: parent.top
    anchors.bottom: parent.bottom

    onVideoLoaded:
    {
      psychoApp.state="ReadyState"
      appLogic.setCurrentFilm(player.videoFile)
      console.log("state: "+psychoApp.state)
    }

    onVideoStarted:
    {
      psychoApp.state="RunningState"
      appLogic.start();
      console.log("state: "+psychoApp.state)
    }

    onVideoPaused:
    {
      psychoApp.state="PauseState"
      console.log("state: "+psychoApp.state)
    }

    onVideoStopped:
    {
      psychoApp.state="ReadyState"
      appLogic.save();
      console.log("state: "+psychoApp.state)
    }
  }

  Rectangle
  {
    id: clickSide
    width: 15
    color: "darkslategrey"
    anchors.bottomMargin: 5
    anchors.topMargin: 5
    anchors.right: buttonContainer.left
    anchors.rightMargin: 5

    anchors.top: parent.top
    anchors.bottom: parent.bottom

    state: "ShownState"

    MouseArea
    {
      id: mouseArea
      anchors.fill: parent
      onClicked:
      {
        if (clickSide.state == "ClosedState")
          clickSide.state = "ShownState"
        else if(clickSide.state == "ShownState")
          clickSide.state = "ClosedState"
      }
    }

    states: [
      State {
        name: "ShownState"

        PropertyChanges{target: buttonContainer; width: 85}
        PropertyChanges{target: buttonContainer; opacity: 1.0}
        PropertyChanges{target: buttonContainer; enabled: true}
      },
      State {
        name: "ClosedState"

        PropertyChanges{target: buttonContainer; width: 0}
        PropertyChanges{target: buttonContainer; opacity: 0.0}
        PropertyChanges{target: buttonContainer; enabled: false}
      }
    ]

    transitions: [
      Transition {
        to: "*"

        ParallelAnimation{
          NumberAnimation{target: buttonContainer; properties: "width"; duration: 1000}
          NumberAnimation{target: buttonContainer; properties: "opacity"; duration: 750}
        }
      }
    ]
  }

  PsyButtonContainer
  {
    id: buttonContainer
    width: 85
    color: "lightgrey"

    anchors.rightMargin: 5
    anchors.bottomMargin: 5
    anchors.topMargin: 5

    anchors.top: parent.top
    anchors.bottom: parent.bottom
    anchors.right: parent.right

    onNewUser: newUserDialog.visible=true;
    onUpClicked: psychoApp.upKey();
    onDownClicked: psychoApp.downKey();
    onSettingsClicked: outputDirDialog.visible=true
  }

  MessageDialog{
    id: messageDialog
    visible: false

    onAccepted: visible=false
  }

  MessageDialog{
    id: availableUserDialog

    title: "user still available"
    text: "a user with this id is still available!\n"
         + "should these user data be overwritten?";
    standardButtons: StandardButton.Yes | StandardButton.No
    visible: false;

    onYes:
    {
      psychoApp.userID=newUserDialog.userID;
      appLogic.newUser(psychoApp.userID)
      buttonContainer.userField.text=psychoApp.userID;
      visible=false;
      newUserDialog.visible=false;
      buttonContainer.newUserAction.text=psychoApp.userID

      if(psychoApp.state=="InitState")
        psychoApp.state="UserState";

      console.log("state:"+psychoApp.state);
    }
    onNo: visible=false;
  }

  FileDialog{
    id: outputDirDialog
    visible: false
    selectFolder: true

    onAccepted:
    {
      psychoApp.outputPath=outputDirDialog.fileUrl
      appLogic.setOutputDir(psychoApp.outputPath);
      visible=false
    }
  }

  NewUserDialog{
    id: newUserDialog
    opacity: 1.0
    anchors.centerIn: player
    visible: false

    onOkClicked:
    {
      if(newUserDialog.userID.trim().length==0)
      {
        messageDialog.title="user id not valid"
        messageDialog.text="please choose a valid user id!"
        messageDialog.visible=true;
      }
      else
      {
        if(appLogic.exists(newUserDialog.userID))
        {
          availableUserDialog.visible=true
        }
        else
        {
          psychoApp.userID=newUserDialog.userID;
          appLogic.newUser(psychoApp.userID)
          buttonContainer.userField.text=psychoApp.userID;
          newUserDialog.visible=false;
          buttonContainer.newUserAction.text=psychoApp.userID

          if(psychoApp.state=="InitState")
            psychoApp.state="UserState";

          console.log("state:"+psychoApp.state);
        }
      }
    }

    onCancelClicked: newUserDialog.visible=false;
  }

  focus: true;
  Keys.onPressed:
  {
    if (event.key == Qt.Key_Up)
    {
      psychoApp.upKey();
      event.accepted = true;
    }
    else if(event.key == Qt.Key_Down)
    {
      psychoApp.downKey();
      event.accepted = true;
    }
  }

  onUserIDChanged:
  {
    if(userID.trim().length==0)
      buttonContainer.userField.visible=false;
    else
      buttonContainer.userField.visible=true;
  }

  states: [
    State {
      name: "InitState"

      PropertyChanges{target: buttonContainer.newUserAction; enabled: true}
      PropertyChanges{target: buttonContainer.settingsAction; enabled: true}
      PropertyChanges{target: buttonContainer.upKeyAction; enabled: false}
      PropertyChanges{target: buttonContainer.downKeyAction; enabled: false}

      PropertyChanges{target: player.loadVideoAction; enabled: false}
      PropertyChanges{target: player.startVideoAction; enabled: false}
      PropertyChanges{target: player.stopVideoAction; enabled: false}
    },
    State {
      name: "UserState"

      PropertyChanges{target: buttonContainer.newUserAction; enabled: true}
      PropertyChanges{target: buttonContainer.settingsAction; enabled: true}
      PropertyChanges{target: buttonContainer.upKeyAction; enabled: false}
      PropertyChanges{target: buttonContainer.downKeyAction; enabled: false}

      PropertyChanges{target: player.loadVideoAction; enabled: true}
      PropertyChanges{target: player.startVideoAction; enabled: false}
      PropertyChanges{target: player.stopVideoAction; enabled: false}
    },
    State {
      name: "ReadyState"

      PropertyChanges{target: buttonContainer.newUserAction; enabled: true}
      PropertyChanges{target: buttonContainer.settingsAction; enabled: true}
      PropertyChanges{target: buttonContainer.upKeyAction; enabled: false}
      PropertyChanges{target: buttonContainer.downKeyAction; enabled: false}

      PropertyChanges{target: player.loadVideoAction; enabled: true}
      PropertyChanges{target: player.startVideoAction; enabled: true}
      PropertyChanges{target: player.stopVideoAction; enabled: false}
    },
    State {
      name: "RunningState"

      PropertyChanges{target: buttonContainer.newUserAction; enabled: false}
      PropertyChanges{target: buttonContainer.settingsAction; enabled: false}
      PropertyChanges{target: buttonContainer.upKeyAction; enabled: true}
      PropertyChanges{target: buttonContainer.downKeyAction; enabled: true}

      PropertyChanges{target: player.loadVideoAction; enabled: false}
      PropertyChanges{target: player.startVideoAction; enabled: true}
      PropertyChanges{target: player.stopVideoAction; enabled: true}
    },
    State {
      name: "PauseState"

      PropertyChanges{target: buttonContainer.newUserAction; enabled: false}
      PropertyChanges{target: buttonContainer.settingsAction; enabled: false}
      PropertyChanges{target: buttonContainer.upKeyAction; enabled: false}
      PropertyChanges{target: buttonContainer.downKeyAction; enabled: false}

      PropertyChanges{target: player.loadVideoAction; enabled: false}
      PropertyChanges{target: player.startVideoAction; enabled: true}
      PropertyChanges{target: player.stopVideoAction; enabled: true}
    }
  ]
}
